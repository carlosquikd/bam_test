<?php
/*
Plugin Name: Simple Ad System
Plugin URI: https://www.linkedin.com/in/carlos-cardenas-carcaras/
Description: Symple banner and ad system.
Version: 0.1
Author: Carlos Cárdenas
Author URI: https://www.linkedin.com/in/carlos-cardenas-carcaras/
License: Personal private use.
*/
register_activation_hook( __FILE__, 'crudOperationsTable');
function crudOperationsTable() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'bamads';
    $sql = "CREATE TABLE `$table_name` (
    `bamad_id` int(11) NOT NULL AUTO_INCREMENT,
    `bamad_type` varchar(150) DEFAULT NULL,
    `bamad_template` varchar(150) DEFAULT NULL,
    `bamad_title` varchar(160) DEFAULT NULL,
    `bamad_date` datetime DEFAULT NULL,
    PRIMARY KEY(bamad_id)
    ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
add_action('admin_menu', 'addAdminPageContent');
function addAdminPageContent() {
    add_menu_page('Ads', 'Symple Ad System', 'manage_options' ,__FILE__, 'sasAdminPage', 'dashicons-wordpress');
}
function sasAdminPage() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'bamads';
    if (isset($_POST['newsubmit'])) {
        $bamad_template = $_POST['bamad_template'];
        $bamad_title = $_POST['bamad_title'];
        $bamad_type = $_POST['bamad_type'];
        $date = strtotime($_POST['bamad_date']);
        $bamad_date = date("Y-m-d H:i:s",$date);
        $wpdb->query("INSERT INTO $table_name(bamad_template,bamad_type,bamad_title,bamad_date) VALUES('$bamad_template','$bamad_type','$bamad_title','$bamad_date')");
        echo "<script>location.replace('admin.php?page=simple-ad-system/simple-ad-system.php');</script>";
    }
    if (isset($_POST['uptsubmit'])) {
        $bamad_id = $_POST['bamad_id'];
        $bamad_template = $_POST['uptbamad_template'];
        $bamad_type = $_POST['uptbamad_type'];
        $bamad_title = $_POST['uptbamad_title'];
        $date = strtotime($_POST['uptbamad_date']);
        $bamad_date = date("Y-m-d H:i:s",$date);
        $wpdb->query("UPDATE $table_name SET bamad_template='$bamad_template',bamad_type='$bamad_type',bamad_title='$bamad_title',bamad_date='$bamad_date' WHERE bamad_id='$bamad_id'");
        echo "<script>location.replace('admin.php?page=simple-ad-system/simple-ad-system.php');</script>";
    }
    if (isset($_GET['del'])) {
        $del_id = $_GET['del'];
        $wpdb->query("DELETE FROM $table_name WHERE bamad_id='$del_id'");
        echo "<script>location.replace('admin.php?page=simple-ad-system/simple-ad-system.php');</script>";
    }
    $plugin_path = plugin_dir_path(__FILE__);
    $theme_directories = glob($plugin_path.'/templates/*' , GLOB_ONLYDIR);
    $themes = array();
    foreach($theme_directories as $theme){
        $themstr = explode('/',$theme);
        $themes[] = end($themstr);
    }
    ?>
    <div class="wrap">
        <h2>Simple Ad System</h2>
        <table class="wp-list-table widefat striped">
            <thead>
                <tr>
                    <th width="3%">Ad ID</th>
                    <th width="15%">Ad Type</th>
                    <th width="15%">Ad Template</th>
                    <th width="15%">Ad Title</th>
                    <th width="15%">Ad End Date</th>
                    <th width="15%">ShortCode</th>
                    <th width="22%">Actions</th>
                </tr>
            </thead>
            <tbody>
                <form action="" method="post">
                    <tr>
                        <td>-</td>
                        <td><input type="text" id="bamad_type" name="bamad_type"></td>
                        <td><select name="bamad_template" id="bamad_template">
                            <?php
                            foreach($themes as $theme){
                                echo "<option name='".$theme."'>".$theme."</option>";
                            }
                            ?>
                        </select>
                        <td><input type="text" id="bamad_title" name="bamad_title"></td>
                        <td><input type="datetime-local" id="bamad_date" name="bamad_date"></td>
                        <td>&nbsp;</td>
                        <td>
                            <button id="newsubmit" name="newsubmit" type="submit" class="button">INSERT</button>
                        </div>
                        </td>

                    </tr>
                </form>
                <?php
                    $result = $wpdb->get_results("SELECT * FROM $table_name");
                    foreach ($result as $print) {
                        echo "
                            <tr>
                                <td width='3%'>$print->bamad_id</td>
                                <td width='15%'>$print->bamad_type</td>
                                <td width='15%'>$print->bamad_template</td>
                                <td width='15%'>$print->bamad_title</td>
                                <td width='15%'>".$print->bamad_date."</td>
                                <td width='15%'>[bamad ad_id=$print->bamad_id]</td>
                                <td width='22%'><a href='admin.php?page=simple-ad-system/simple-ad-system.php&upt=$print->bamad_id' ><button type='button' class='button'>UPDATE</button></a> <a href='admin.php?page=simple-ad-system/simple-ad-system.php&del=$print->bamad_id' onclick=\"return confirm('Are you sure?')\"><button type='button' class='button'>DELETE</button></a></td>
                            </tr>
                        ";
                    }
                ?>
            </tbody>
        </table>
        <br>
        <br>
        <?php
            if (isset($_GET['upt'])) {
                $upt_id = $_GET['upt'];
                $result = $wpdb->get_results("SELECT * FROM $table_name WHERE bamad_id='$upt_id'");
                foreach($result as $print) {
                    $name = $print->name;
                    $bamad_template = $print->bamad_template;
                    $bamad_title = $print->bamad_title;
                    $bamad_date = date('Y-m-dTH:m',strtotime($print->bamad_date));
                    $bamad_date = str_replace('UTC','T',$bamad_date);
                }
                echo "
                <table class='wp-list-table widefat striped'>
                    <thead>
                        <tr>
                            <th width=\"3%\">Ad ID</th>
                            <th width=\"15%\">Ad Type</th>
                            <th width=\"15%\">Ad Template</th>
                            <th width=\"15%\">Ad Title</th>
                            <th width=\"15%\">Ad End Date</th>
                            <th width=\"15%\">&nbsp;</th>
                            <th width=\"22%\">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action='' method='post'>
                            <tr>
                                <td width='3%'>$print->bamad_id <input type='hidden' id='uptid' name='bamad_id' value='$print->bamad_id'></td>
                                <td width='15%'><input type='text' id='uptbamad_type' name='uptbamad_type' value='$bamad_type'></td>
                                <td width='15%'>";
                                echo '<select name="uptbamad_template" id="uptbamad_template">';

                                    foreach($themes as $theme){
                                        $selected = '';
                                        if($bamad_template == $theme)
                                            $selected = 'selected';
                                        echo "<option name='".$theme."' ".$selected.">".$theme."</option>";
                                    }

                                echo '</select>';
                echo "          </td>
                                <td width='15%'><input type='text' id='uptbamad_title' name='uptbamad_title' value='$bamad_title'></td>
                                <td width='15%'><input type='datetime-local' id='uptbamad_date' name='uptbamad_date' value='$bamad_date'></td>
                                <td width='15%'>[bamad ad_id=$print->bamad_id]</td>
                                <td width='22%'><button id='uptsubmit' name='uptsubmit' type='submit'>UPDATE</button> <a href='admin.php?page=simple-ad-system/simple-ad-system.php'><button type='button'>CANCEL</button></a></td>
                            </tr>
                        </form>
                    </tbody>
                </table>";
            }
        ?>
    </div>
    <?php
}

function bamad_shortcode($atts = [], $content = null, $tag = '')
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bamads';
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $plugin_path = plugin_dir_path(__FILE__);
    $bamad_atts = shortcode_atts([
                                     'ad_id' => '4', //use 4 by default
                                 ], $atts, $tag);

    $id = esc_html__($bamad_atts['ad_id'], 'bamad');
    $ad_info = $wpdb->get_results("SELECT * FROM $table_name WHERE bamad_id='$id'");
    if ( null === $ad_info ) {
        return '';
    }
    foreach($ad_info as $ad_data) {

        $name = $ad_data->name;
        $bamad_template = $ad_data->bamad_template;
        $bamad_title = $ad_data->bamad_title;
        $bamad_date = $ad_data->bamad_date;
    }

    $term = get_the_category();
    switch ($term['0']->slug) {
        case 'NFL':
        case 'nfl':
            $bg_color = 'black';
            break;
        case 'NBA':
        case 'nba':
            $bg_color = 'orange';
            break;
        case 'MLB':
        case 'mlb':
            $bg_color = 'blue';
            break;
        default:
            $bg_color = 'gray';
            break;
    }

    $template_string = file_get_contents($plugin_path.'templates/'.$bamad_template.'/template.html');
    $template_content = plugin_dir_url(__FILE__).'templates/'.$bamad_template.'/';
    $search = array('REPL_TEMPLATE_DIR','REPL_TITLE','REPL_DATE','REPL_BG_COLOR');
    $replace = array($template_content,$bamad_title,$bamad_date,$bg_color);
    $template_string = str_replace($search,$replace,$template_string);
    // return output
    return $template_string;
}

function bamad_shortcodes_init()
{
    add_shortcode('bamad', 'bamad_shortcode');
}

add_action('init', 'bamad_shortcodes_init');
